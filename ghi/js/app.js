window.addEventListener('DOMContentLoaded', async () => {
    function createCard(name, location, description, pictureUrl, starts, ends) {
        return `
            <div class="card mb-3 shadow">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${new Date(starts).toLocaleDateString()} -
                    ${new Date(ends).toLocaleDateString()}
            </div>
        `;
    }

    function createAlert(error) {
        return `
        <div class="alert alert-danger" role="alert">
            ${error}
        </div>
        `
    }

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            // if the response is bad
            throw new Error('Bad response from API.');

        } else {
            const data = await response.json();

            let index = 0
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const title = details.conference.name;
                  const location = details.conference.location.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const starts = details.conference.starts;
                  const ends = details.conference.ends;
                  const html = createCard(title, location, description, pictureUrl, starts, ends);
                  console.log(html);
                  const column = document.querySelector(`#col-${index}`);
                  column.innerHTML += html;
                  index += 1
                  if (index > 2) {
                    index = 0
                  }

                }
              }

            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();

            //     //Conference Description
            //     const desc = details.conference;
            //     const confDescription = document.querySelector('.card-text');
            //     confDescription.innerHTML = desc.description

            //     //image
            //     const img = details.conference;
            //     const cardImg = document.querySelector('.card-img-top');
            //     cardImg.src = img.location.picture_url;
            //     console.log(details);
            // }

        }

    } catch (e) {
        // if an error is raised
        console.error(e)
        const alert = createAlert(e)
        const alertDiv = document.querySelector(`#alertAPI`);
        alertDiv.innerHTML = alert
    }
});
