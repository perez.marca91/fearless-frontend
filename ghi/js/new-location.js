window.addEventListener('DOMContentLoaded', async () => {

    const stateUrl = "http://localhost:8000/api/states/";
    try {
        const response = await fetch(stateUrl);
        const stateSelect = document.getElementById('state');
        if (!response.ok) {
            throw new Error('Bad response from State API')
        } else {
            const data = await response.json();

            for (let state of data.states) {
                const option = document.createElement('option')
                option.value = state.abbreviation
                option.innerHTML = state.name
                stateSelect.appendChild(option)
            }
            console.log(data);
        }
    } catch (e){
        console.error(e)
    }

    // State fetching code, here...

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        console.log('need to submit the form data');
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
    });

})
